#!/bin/bash
# To be able to execute this script, you need to install the policycoreutils-python package
# E.g: yum install policycoreutils-python -y
# To create a policy, first disable selinux, and perform all the actions that you want galera to permit:
# For example create a database, create a cluster etc. etc.
# Selinux writes all performed actions to var/log/audit.log
# When you have performed all the neccesary actions, run this script, and your policy will be created

echo "Creating textease file from selinux logging"
fgrep "mysqld" /var/log/audit/audit.log | audit2allow -m galera -o galera.te

echo "Compiling the textease file into a SELinux policy module"
checkmodule -M -m galera.te -o galera.mod

echo "Creating a package from the policy module"
semodule_package -m galera.mod -o galera.pp

echo "Load the package into SELinux"
semodule -i galera.pp


