#!/bin/bash

echo "This database server is managed by automation. Debian start scripts do not arrange mysql_upgrade. Checking of tables is not needed as you should only use InnoDB."

exit 0
