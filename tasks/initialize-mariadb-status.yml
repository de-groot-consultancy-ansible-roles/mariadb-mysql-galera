- name: Gather facts for all cluster members (regardless of limit or tags)
  setup:
  delegate_to: "{{ item }}"
  delegate_facts: True
  when: hostvars[item]['ansible_default_ipv4'] is not defined
  loop: "{{ ansible_play_hosts if mariadb_galera_enabled else [inventory_hostname] }}"

- name: "Check if any other node in this playbook are currently running Galera cluster"
  shell: mysql -Ee "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status'" | grep "Value:" | grep -v "Disconnected" | wc -l
                                                    # ^^ One liner that prints "1" if there is a galera cluster ^^
                                                    # If the node is non-primary, a galera cluster is still detected. Hopefully Galera 4 functionality will get the nodes back together.
  register: check_other_node_is_cluster
  delegate_to: "{{ item }}"                         # Run these checks on the _OTHER_ cluster membmers
  loop: "{{ ansible_play_hosts }}"            # Make sure you specify the entire cluster in this playbook.
  changed_when: check_other_node_is_cluster.rc == 0 and check_other_node_is_cluster.stdout == "1"
  when: 
   - item != inventory_hostname # If this node has a cluster we still need to reinitiate after a restart. Therefore check not needed on this node
   - mariadb_galera_enabled

- name: "Check if this node is a single node cluster and operational"
  shell: mysql -Ee "SHOW GLOBAL STATUS LIKE 'wsrep_cluster_status'" | grep "Value:" | grep -v "non-Primary" | wc -l
                                                    # ^^ One liner that prints "1" if there is a galera cluster ^^
                                                    # If the node is non-primary, a galera cluster is still detected. Hopefully the nodes will rejoin when this node is started.
  register: db_single_node_cluster_check_command
  changed_when: db_single_node_cluster_check_command.rc == 0 and db_single_node_cluster_check_command.stdout == "1"
  when: 
   - mariadb_galera_enabled

- name: "Store if one of the databases is operational or if this node is a single node cluster"
  set_fact:
    is_cluster_member: "{{ check_other_node_is_cluster is changed }}"
    is_single_node_cluster: "{{ db_single_node_cluster_check_command is changed }}"

- name: "RedHat based systems - Check if MariaDB is installed"
  shell: "rpm -qa|egrep '(MariaDB-server|galera)'|wc -l"
  register: is_installed_command_redhat
  changed_when: is_installed_command_redhat.rc == 0 and is_installed_command_redhat.stdout | int > 1
  when: ansible_os_family == "RedHat"

- name: "Debian based systems - Check if MariaDB is installed"
  shell: "dpkg -l|egrep '(mariadb-server|galera)'|wc -l"
  register: is_installed_command_debian
  changed_when: is_installed_command_debian.rc == 0 and is_installed_command_debian.stdout | int > 1
  when: ansible_os_family == "Debian"

- name: "Store if MariaDB is installed"
  set_fact:
    is_installed: "{{ is_installed_command_redhat is changed or is_installed_command_debian is changed }}"

- name: "RedHat based systems - Check if MariaDB needs to be upgraded"
  shell: "yum --disableplugin versionlock -y check-update MariaDB-server galera 2>/dev/null|egrep '(MariaDB-server|galera)'|wc -l"
  register: needs_update_command_redhat
  changed_when: needs_update_command_redhat.rc == 0 and needs_update_command_redhat.stdout | int > 0
  when: ansible_os_family == "RedHat"

- name: Update apt-get repo and cache
  apt:
    update_cache: yes
    force_apt_get: yes
    cache_valid_time: 60
  when: ansible_os_family == "Debian"

- name: "Debian based systems - Check if MariaDB needs to be upgraded"
  shell: "apt list --upgradable|egrep '(mariadb-server|galera)'|wc -l"
  register: needs_update_command_debian
  changed_when: needs_update_command_debian.rc == 0 and needs_update_command_debian.stdout | int > 0
  when: ansible_os_family == "Debian"

- name: "Debian based systems - Check if MariaDB needs a major upgrade"
  shell: "dpkg -l|grep 'mariadb-server'|grep '{{ mariadb_version }}'|wc -l"
  register: needs_major_update_command_debian
  changed_when: (needs_major_update_command_debian.rc == 0 and needs_major_update_command_debian.stdout | int == 0) and is_installed
  when: ansible_os_family == "Debian"

- name: "Store if MariaDB or Galera need to be updated"
  set_fact:
    needs_update: "{{ needs_update_command_debian is changed or (needs_major_update_command_debian and needs_major_update_command_debian is changed) or needs_update_command_redhat is changed }}"

- name: "Print if MariaDB needs to be upgraded"
  debug:
    msg: "MariaDB needs to be Updated: {{ needs_update }}"

# Jain@OceanDBA 2021-12-14 - Verify is ZFS is installed and check is ZFS is online
- name: "Check if zfs is enabled"
  shell: zpool status -v  | grep 'state:' | awk '{print $2}'
  register: zpool_command_output
  changed_when: zpool_command_output.rc == 0 and zpool_command_output.stdout == "ONLINE"


- name: "Store if ZFS configuration should be loaded or not"
  set_fact:
    zfs_enabled: "{{ zpool_command_output is changed }}"

- name: "Print cluster status"
  debug:
    msg: "Host {{ inventory_hostname}}, is_installed: {{ is_installed }}, galera_enabled: {{ mariadb_galera_enabled }}, is_cluster_member: {{ is_cluster_member }}, is_single_node_cluster: {{ is_single_node_cluster }}, zfs_enabled: {{ zfs_enabled }}, needs_update: {{ needs_update }}"
