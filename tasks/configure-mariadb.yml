################ TO GALERA OR NOT TO GALERA ####################
- name: "Add galera configuration files if galera is enabled"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_galera_configuration_files }}"
  when: mariadb_galera_enabled

################ ASYNC REPLICATION OR NOT ####################
- name: "Add replication configuration files if replication is enabled"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_replication_configuration_files }}"
  when: mariadb_replication_enabled


################ ZFS CONFIGURATION OR NOT  ####################
- name: "Add zfs configuration files if filesystem is zfs"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_zfs_configuration_files }}"
  when: zfs_enabled

################ SSL CONFIGURATION OR NOT  ####################
- name: "Add SSL configuration files if enabled"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_ssl_configuration_files }}"
  when: mariadb_ssl_enabled

################ SSL CONFIGURATION OR NOT  ####################
- name: "Add data at rest configuration files if enabled"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_data_at_rest_configuration_files }}"
    mariadb_systemd_desired_configuration_files: "{{ mariadb_systemd_desired_configuration_files + ['aws-kms.env'] }}"
  when: mariadb_data_at_rest_encryption_enabled is truthy

################ OPTIONAL CONFIGURATION FILES ####################
- name: "Add optional configuration files that are enabled"
  set_fact:
    mariadb_configuration_files: "{{ mariadb_configuration_files + mariadb_enabled_optional_configuration_files }}"
  when: mariadb_enabled_optional_configuration_files

- name: "Set configuration directory on Red Hat based systems"
  set_fact: 
    configuration_directory: "/etc/my.cnf.d"
    ssl_directory: "/etc/mariadb-ssl-certificates"
  when: ansible_os_family == "RedHat"

- name: "Set configuration directory on Debian based systems"
  set_fact: 
    configuration_directory: "/etc/mysql/conf.d"
    ssl_directory: "/etc/mysql/mariadb-ssl-certificates"
  when: ansible_os_family == "Debian"

  #################### GALERA LIBRARY PATH ####################################
- name: "Set galera 3 library path for Red Hat based systems"
  set_fact:
    galera_library_path: "/usr/lib64/galera/libgalera_smm.so"
  when: ansible_os_family == "RedHat" and mariadb_version is version('10.3', '<=')

- name: "Set galera 4 library path for Red Hat based systems"
  set_fact:
    galera_library_path: "/usr/lib64/galera-4/libgalera_smm.so"
  when: ansible_os_family == "RedHat" and mariadb_version is version('10.4', '>=')

- name: "Set galera 3 library path for Debian based systems"
  set_fact:
    galera_library_path: "/usr/lib/galera/libgalera_smm.so"
  when: ansible_os_family == "Debian" and mariadb_version is version('10.3', '<=')

- name: "Set galera 4 library path for Debian based systems"
  set_fact:
    galera_library_path: "/usr/lib/galera/libgalera_smm.so"
  when: ansible_os_family == "Debian" and mariadb_version is version('10.4', '>=')

################ AUTO GENERATE SERVER ID WHEN NEEDED ####################
- name: "Generate server id when it is not defined"
  when: mariadb_server_id is falsy
  block:
    - name: "Use shell command to generate server id"
      shell: "echo -n {{ inventory_hostname | escape }} | gzip -c | tail -c8 | hexdump -n4 -e '\"%u\"'"
      register: generate_server_id

    - name: Store the server_id generated from the command
      set_fact:
        mariadb_server_id: "{{ generate_server_id.stdout_lines[0] }}"

################ AUTO GENERATE WSREP_GTID_DOMAIN_ID WHEN NEEDED ####################
- name: "Generate wsrep_gtid_domain_id when it is not defined"
  when: mariadb_wsrep_gtid_domain_id is falsy and mariadb_galera_enabled
  block:
    - name: "Use shell command to generate wsrep_gtid_domain_id"
      shell: "echo -n {{ mariadb_galera_cluster_name | escape }} | gzip -c | tail -c8 | hexdump -n4 -e '\"%u\"'"
      register: generate_wsrep_gtid_domain_id

    - name: Store the wsrep_gtid_domain_id generated from the command
      set_fact:
        mariadb_wsrep_gtid_domain_id: "{{ generate_wsrep_gtid_domain_id.stdout_lines[0] }}"

###### DETERMINE RAMDISK SIZE ###################
- name: "Set default value for ramdisk size"
  set_fact:
    galera_cache_ramdisk_size_mb: 0

- name: "Set ramdisk size when specified in MBs"
  set_fact:
    galera_cache_ramdisk_size_mb: "{{ (mariadb_galera_cluster_ist_size | replace('M', '') | int) + mariadb_data_at_rest_gcache_ramdisk_spare_size_mb }}"
  when: "'M' in mariadb_galera_cluster_ist_size and mariadb_data_at_rest_gcache_ramdisk_enabled and mariadb_galera_enabled"

- name: "Set ramdisk size when specified in GBs"
  set_fact:
    galera_cache_ramdisk_size_mb: "{{ ((mariadb_galera_cluster_ist_size | replace('G', '') | int) * 1024) + mariadb_data_at_rest_gcache_ramdisk_spare_size_mb }}"
  when: "'G' in mariadb_galera_cluster_ist_size and mariadb_data_at_rest_gcache_ramdisk_enabled and mariadb_galera_enabled"

######## CALCULATE BUFFER POOL SIZE###############
- name: "Autosize buffer pool on"
  set_fact:
    mariadb_innodb_buffer_pool_size: "{{ ((ansible_memtotal_mb | int - galera_cache_ramdisk_size_mb | int) * ((mariadb_innodb_buffer_pool_percentage | int) / 100)) | int }}M"
  when: mariadb_innodb_buffer_pool_size is falsy

################ UPDATE CONFS ####################
- name: "Get currently present configuration files"
  shell: "ls -1 {{ configuration_directory }}"
  register: contents

- name: "Delete unwanted configuration files"
  file:
    path: "{{ configuration_directory }}/{{ item }}"
    state: absent
  register: deleted_unwanted_configuration_files
  with_items: "{{ contents.stdout_lines }}"
  when: item not in mariadb_configuration_files

################ (UN)WANTED DEBIAN CONFS ####################
- name: "Fix (un)wanted debian confs"
  when: ansible_os_family == "Debian"
  block: 

  - name: "Get currently present files in debian configuration root"
    shell: "ls -1 {{ configuration_directory }}/../"
    register: contents

  - name: "Delete unwanted files in debian configuration root"
    file:
      path: "{{ configuration_directory }}/../{{ item }}"
      state: absent
    loop: "{{ contents.stdout_lines }}"
    register: deleted_unwanted_debian_configuration_root_files
    when: item not in ('my.cnf', configuration_directory | basename, 'debian-start', 'mariadb-ssl-certificates', 'mariadb.cnf')

  - name: "Install arbitrary debian-start file for debian start-up process to work"
    copy: 
      src: "files/debian-start.sh"
      dest: "{{ configuration_directory }}/../debian-start"
      mode: 0755

- name: "Deploy Common configuration file (all versions, all storage engines)"
  template:
    src: "{{ item }}.j2"
    dest: "{{ configuration_directory }}/{{ item }}"
  register: changed_configuration_files
  with_items: "{{ mariadb_configuration_files }}"

- name: "MariaDB - Deploy configuration files for mariadb upgrade compatibility for versions <= 10.3"
  when: mariadb_version is version('10.3', '<=')
  block:
  - name: "MariaDB - Deploy my.cnf and symlink to mariadb.cnf"
    template:
      src: "etc_my.cnf.j2"
      dest: "{{ configuration_directory }}/../my.cnf"
    register: changed_main_config_file_old

  - name: "MariaDB - Symlink my.cnf to mariadb.cnf"
    file:
      src: "{{ configuration_directory }}/../my.cnf"
      dest: "{{ configuration_directory }}/../mariadb.cnf"
      force: yes
      state: link

- name: "MariaDB - Deploy configuration files for mariadb upgrade compatibility for versions >=10.4"
  when: mariadb_version is version('10.4', '>=')
  block:
  - name: "MariaDB - Deploy mariadb.cnf and symlink to my.cnf"
    template:
      src: "etc_my.cnf.j2"
      dest: "{{ configuration_directory }}/../mariadb.cnf"
    register: changed_main_config_file_new

  - name: "MariaDB - Symlink mariadb.cnf to my.cnf"
    file:
      src: "{{ configuration_directory }}/../mariadb.cnf"
      dest: "{{ configuration_directory }}/../my.cnf"
      state: link
      force: yes

- name: "Install systemd configuration file"
  template:
    src: "systemd.conf.j2"
    dest: "{{ mariadb_systemd_configuration_directory }}/99-ansible.conf"
  register: systemd_config_file

- name: "Register systemd configuration files currently present"
  shell: "ls -1 {{ mariadb_systemd_configuration_directory }}/"
  register: systemd_contents

- name: "Delete unwanted files in systemd configuration directory"
  file:
    path: "{{ mariadb_systemd_configuration_directory }}/{{ item }}"
    state: absent
  loop: "{{ systemd_contents.stdout_lines }}"
  register: deleted_systemd_configuration_files
  when: item not in mariadb_systemd_desired_configuration_files 

- name: "Daemon reload if systemd configuration has changed"
  systemd:
    daemon_reload: yes
  when: systemd_config_file is changed or deleted_systemd_configuration_files is changed

- name: "Change running configuration for wsrep_gtid_domain_id to work around MDEV-34440"
  shell: "mysql -e 'SET GLOBAL wsrep_gtid_domain_id={{ mariadb_wsrep_gtid_domain_id }}'"
  loop: "{{ ansible_play_hosts }}"
  delegate_to: "{{ item }}"
  when: item != inventory_hostname and mariadb_galera_enabled
  ignore_errors: True

- name: "Stop MariaDB on config change"
  import_tasks: stop-mariadb.yml
  when: (deleted_unwanted_configuration_files is changed or changed_configuration_files is changed or changed_main_config_file_old is changed or changed_main_config_file_new is changed or (ansible_os_family == 'Debian' and deleted_unwanted_debian_configuration_root_files is changed) ) and is_installed

- name: "Install logrotate configuration file"
  template:
    src: "logrotate.conf.j2"
    dest: "/etc/logrotate.d/mariadb"
  register: logrotate_config_changed

- name: "Remove obsolte logrotate configurations"
  file:
    path: "/etc/logrotate.d/{{ item }}"
    state: absent
  loop:
    - mysql
    - mysql.server
    - mysql-server

- name: "Install logrotate"
  package:
    name: "logrotate"
    state: present

- name: "Restart logrotate if needed"
  service:
    name: logrotate
    state: restarted
  when: logrotate_config_changed is changed
