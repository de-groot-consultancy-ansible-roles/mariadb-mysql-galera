# MariaDB role

## About

This role deals with the install and configurations of a MariaDB cluster on Red Hat (CentOS, Fedora, AlmaLinux, etc) and Debian (Debian, Ubuntu, etc) based systems. The credentials part of this role assumes there is only 1 application running on the system, therefore the development role has access to all databases.

This role installs MariaDB Galera Cluster at the specified major version (_mariadb_version_), installs major and minor upgrades, optionally configures data at rest and data in motion encryption, optionally installs an arbitrator, adds new local users to the running installation and (optionally) update their passwords and (optionally) remove users that are not specified in the automation. By default, a cluster is created to the nodes or group you execute the role to in a single play, but you can specify _mariadb_galera_enabled_ as _False_ to install (a) single instance(s).

Check out our [example simple inventory file](examples/simple/inventory.ini) and [example simple playbook](examples/simple/playbook-simple.yml).

## Installing
When using Galaxy add it to your requirements file:
```
- src: git+https://gitlab.com/de-groot-consultancy-ansible-roles/mariadb-mysql-galera.git
  name: mariadb-mysql-galera
  version: "main" or "4.2" or "branch name"
```
Or clone the role to your local directory.

## Required parameters:
- mariadb_galera_cluster_name: "my_production_cluster" 
- or: mariadb_galera_enabled: False

## Changing the cluster name
Please note that when changing the cluster name all nodes except the first node should be stopped. Run the role in check node to find out to which node it would be executed first. It is not possible to change the cluster name without a short outage. When using this procedure and changing the cluster name with automation the outage is approximately 2 minutes.

## Supported tags
These tags are supported by this role. Each of them you can skip or run separately. 
In theory at least, but don't expect a new installation to work if you do not enable configuration management.
Supported tags:
  - installation_and_upgrades: Installs and upgrades MariaDB
  - configuration_management: Removes incorrect / unwanted configuration, install configuration files
  - certificate_management: Install SSL certificates if specified
  - os_management: Upgrade all packages, reboot if needed, Set up firewalld if enabled, selinux if running on a red hat based system, fix incorrect mysql service, disable apparmor on Debian systems, disable swap (if Galera is enabled)
  - user_management: Adds users (if mariadb_create_local_users is enabled, by default on stand-alone nodes and first node in cluster), creates and modifies roles, drops incorrect users and roles if enabled (mariadb_update_local_users, FALSE by default)
  - arbitrator_management: Installs and upgrades the galera arbitrator
  - move_directories_to_new_location: Move data directory, binary logs and innodb files to new directory if specified. Use with care, it should be supported to continue moving after half way done but this is not guaranteed.
 
## Optional parameters:
**For a complete list, check defaults/main.yml** - most notable parameters are listed here:
- mariadb_galera_enabled: (default: true when executing to multiple hosts, or false when executing to 1 host) boolean to say Galera is enabled or not
- mariadb_firewalld_enabled: (default: true) boolean to say Firewall is enabled or not. By default we add the rules and manage the service. Managing the service can be disabled by setting _mariadb_firewalld_manage_service_ to False.
- mariadb_redhat_selinux_enabled: (default: true) boolean to say SELinux is enabled or not

## Graceful switchovers
For Graceful switchovers, ProxySQL is supported. Pull requests for other proxies are welcome.
Before shutdown, the role logs in to a group of servers and changes the weight of the server currently being modified to 0. This removes it as a writer and switches the reader connections to another node.
Then, we wait 10 seconds (configurable). If extra wait users are configured we wait until all connections from those users have dissapeared for up to 300 (configurable) seconds. Configuration parameters:
mariadb_graceful_switchover_proxysql_hosts: (default: empty list) hots to apply the graceful switchover with, for example: "{{ groups['proxysql'] }}"
mariadb_graceful_switchover_proxy_node_address: (default: mariadb_wsrep_node_address or ansible_host) configured address (we recommend to use ip address) of node to switch over in proxy configuration
mariadb_graceful_switchover_default_wait_time: (default: 10) seconds to wait for switchover of all users
mariadb_graceful_switchover_extra_wait_users: (default: empty list) users to wait the extra time for. If there are no more connections from these user, the graceful shutdown has completed.
mariadb_graceful_switchover_extra_wait_time: (default: 300) maximum time (in seconds) to wait for the extra_wait_users to dissapear. If this tim has passed, the graceful shutdown has completed.
**WARNING**: If the role execution stops in the middle (before the switch back), reconfigure the ProxySQL instances or execute 'LOAD MYSQL SERVERS TO MEMORY; LOAD MYSQL SERVERS TO RUNTIME;' on the ProxySQL instance(s). Executing the role successfully should also restore the weight of the ProxySQL instances.

## User management:
** By default, only new users are added and passwords of existing specified users are updated. Incorrect users are not removed**. The automation _always adds the SST user and root@localhost account_. Both accounts (by default) do not have a password and use authsocket authentication.

Specify _user_management_ tag to only execute user management and skip upgrading/reconfiguring the database: _ansible-playbook -i environments/production -t user_management cluster.yml_

Recommendations:
- Configure the role to remove incorrect users (see configuration parameter below). Incorrect as in: Users that are not specified in the automation
- Add local services (for example: monitoring) with authsocket authentication; see 'zabbix' user in example
- Use roles and specify named access for humans (global developer roles are added by default)
- Configure each developer's default role to the read only role for extra safety

Configuration parameters: 

- __mariadb_local_users__:
```
mariadb_local_users:
 - username: "named_senior_developer"
   password: "*PASSWORD HAS FROM SELECT PASSWORD('password');"
   priv: "*.*:USAGE"
   roles:
     - devel_all
     - devel_ro
   default_role: "devel_ro"
   host: "%"
   encrypted: yes
 - username: "named_junior_developer"
   password: "*PASSWORD HAS FROM SELECT PASSWORD('password');"
   priv: "*.*:USAGE"
   roles:
     - devel_ro
   default_role: "devel_ro"
   host: "%"
   encrypted: yes
 - username: "proxysql-monitoring"
   password: "UnencryptedMonitoringPassword"
   encrypted: no
   host: "%"
   priv: "*.*:USAGE"
 - username: "zabbix"
   password: "{{ ('ThisPasswordWillNotBeUsed' + (10000000000 | random)) | sha1 }}"
   encrypted: no
   host: "localhost"
   priv: "*.*:USAGE"
 - username: "app"
   password: "*PASSWORD HAS FROM SELECT PASSWORD('password');"
   encrypted: yes
   host: "%"
   priv: "app.*:SELECT,INSERT,UPDATE,DELETE,EXECUTE"
 - username: "migration"
   password: "*PASSWORD HAS FROM SELECT PASSWORD('password');"
   encrypted: yes
   host: "%"
   priv: "app.*:ALL"
```
- mariadb_create_local_users: (default: enabled on initial installation of local nodes and initial installation of first member of cluster) Create local users that have access to the database. Please note that this is only executed at the first run. We recommend to use mariadb_update_local_users to apply user management. Enabling this parameter will only add new users and update passwords of existing users.
- mariadb_update_local_users: (default: *disabled*) Create or update local users regardless of cluster or installation state. This will also switch on mariadb_remove_local_unspecified_users.
- mariadb_remove_local_unspecified_users: (default: take value from __mariadb_update_local_users__) remove unspecified users from the system (or add an extra priviledge to find them, when debug mode is eanbled)
- mariadb_remove_unspecified_local_users_debug_mode: (default: *True*) Instead of deleting the localuser, add a column-based permission for the user. Use this to verify the role will not delete any user accounts that matter.

## Data in motion encryption
The role supports deploying SSL certificatates. Please read the documentation carefully and take the appropriate security measures to safely generate and store your SSL certificates.

Some questions to ask yourself: Where is the CA certificate? Where is the CA private key and it's passphrase? Where is the certificate stored? Where is the private key? Is the private key passwordless? Does each node have their own certificate certificate?

We recommend to install an ACMEv2 server to generate the certificates automatically, and implement acmev2 in this role. Each time the automation runs, the certificate should be renewed. We are happy to help implement this but as it is quite a lot of work the implementation is not yet planned by us.

A start could be made by supporting a service like letsencrypt. This way, certificates could be generated by the automation as if there is an acme service already out there. The code could be reused in the future if we support installation and management of acme service.
Pull requests are welcome.

Configure these variables to enable SSL support:

- mariadb_ssl_enabled: False
- mariadb_require_secure_transport: False - Require secure transport - All clients must have SSL enabled. Client certificates are not required.

Next to that, we need to set to generate certificates and store them safely on the control host. In the automation we specify the string contents of the SSL certificate, and we use lookup() template function to load certificate from file on control host file system.
- mariadb_ssl_ca_crt: "{{ lookup('file', '/local/path/to/ca-certificate.crt') }}"  - Use the same CA for both server and client certificates to be able to enable certification verification. Galera and SST can use a differnet CA if needed.
- mariadb_ssl_crl: False # "{{ lookup('/local/path/to/revocation-list.pem') }}"  # Can be left False. Documentation: https://mariadb.com/kb/en/secure-connections-overview/#certificate-revocation-lists-crls - Only supported with OpenSSL (default)   # FIXME: Revocation lists not yet supported for now. Only needed when using public ACME service. When you want to revocate certificates, just generate a new CA and all certificates.
mariadb_ssl_server_crt: False # "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '.crt') }}"
mariadb_ssl_server_key: False # "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '.key') }}"
mariadb_ssl_client_crt: False # "{{ lookup('file', '/local/path/to/client-' + inventory_hostname + '.crt') }}"   # This client certificate is only deployed on the server. Use it as a configuration example but generate a different client certificate for each client connecting to MariaDB.
mariadb_ssl_client_key: False # "{{ lookup('file', '/local/path/to/client-' + inventory_hostname + '.key') }}"

**OPTIONAL** Galera SSL configuration parameters. By default everything is enabled if mariadb_ssl_enabled is True. By default it uses the same certificates.
  
You can configure these parameters to have different configuration parameters for Galera. This can be useful when migrating for non-SSL to SSL galera. Currently the role does not support migrating to SSL. If you want to change a running cluster to SSL with only the automation, shut down all nodes but the first one and run the automation normally. This will cause a short outage in the cluster. You can optionally configure these SSL parameters:
- mariadb_ssl_galera_enabled: "{{ mariadb_ssl_enabled | default(False) }}" - Enable SSL for galera communication
- mariadb_ssl_galera_ca_crt: "{{ mariadb_ssl_ca_crt }}" - or: "{{ lookup('file', '/local/path/to/ca-certificate_' + galera_cluster_name + '.crt') }}" - Optionally you can use a different CA for Galera communication. Make sure the CA used to generate the certificates for each galera node is the same. 
- mariadb_ssl_galera_crt: "{{ mariadb_ssl_server_crt }}" - or: "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '-galera.crt') }}"; Optionally you can use a different certificate for galera communciation but we recommend you to use   the same.
- mariadb_ssl_galera_key: "{{ mariadb_ssl_server_key }}" - or: "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '-galera.key') }}"
  
**OPTIONAL** Galera SST SSL configuration. Uses the the Galera settings by default. Please note that SSL must be configured on both the donor and the joiner.
- mariadb_ssl_galera_mariabackup_sst_enabled: "{{ mariadb_ssl_galera_enabled | default(False) }}"  # Enable SSL for SST communication
- mariadb_ssl_galera_mariabackup_sst_ca_crt: "{{ mariadb_ssl_galera_ca_crt }}" - or: "{{ lookup('file', '/local/path/to/ca-certificate_' + galera_cluster_name + '-sst.crt') }}"; Optionally you can use a different CA for Galera SST as well as CA       communication. Make sure the CA used to generate the certificates for each galera node is the same. 
- mariadb_ssl_galera_mariabackup_sst_crt: "{{ mariadb_ssl_galera_crt }}" - or: "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '-galera-sst.crt') }}"; Optionally you can use a different certificate for galera SST but we recommend you to use the same.
- mariadb_ssl_galera_mariabackup_sst_key: "{{ mariadb_ssl_galera_key }}" - or: "{{ lookup('file', '/local/path/to/server-' + inventory_hostname + '-galera-sst.key') }}"

## Data at rest encryption
Data at rest encryption is (currently) only supported via the AWS Key Management plugin. The plugin is compiled on the target host (at the time of writing not supported on Ubuntu >= 22.04. It is supported to enable data at rest encryption on running instances, but the migration otherwise is not done by the role. Some things to take into consideration:
- Tables are not encrypted (ALTER TABLE db.table ENGINE=InnoDB ENCRYPTED=YES will fix it, rolling schema change can be used)
- New tables are encrypted based on the configuration (mariadb_data_at_rest_encryption_innodb_encrypt_tables, default = ON, means all new tables will be encrypted by default)
- New binary logs are encrypted, old binary logs are not
- Audit plugin cannot create encrypted output. Send it to syslog and configure the protection there instead. 
- File-based general query log and slow query log cannot be encrypted (MDEV-9639). 
- The Aria log is not encrypted (MDEV-8587). This affects only non-temporary Aria tables though.
- The MariaDB error log is not encrypted. The error log can contain query text and data in some cases, including crashes, assertion failures, and cases where InnoDB write monitor output to the log to aid in debugging. It can be sent to syslog too, if needed.
- The redo log (ib_logfile*) is encrypted when migrating from an unencrypted data dir on at least MariaDB 10.6. 
- The ibdata1 file is not encrypted when migrationg from an unencrypted data dir. The question is if this is relevant as the undo log is now created in separate files. **FIXME**
- Therefore: Undo log might be encrypted **FIXME**
- The galera.cache file (it is a file on disk!) is not encrypted. The cheapest solution for this is to put it in a RAM disk, pull requests for this are welcome.

Notable variables: Check defaults/main.yml, from mariadb_data_at_rest_encryption_enabled until mariadb_data_at_rest_encryption_aws_region.

Please note: The AWS key management plugin is stored on the control host (controlled by mariadb_data_at_rest_encryption_control_host_cache_enabled). Some tasks are delegated to the control host, so ansible needs (by default) the same access to the control host as to the target nodes. There are 3 options: (1) Target host is configured without sudo (direct root login), (2) the control host to have the same sudo configuration as the target host, or (3) the control host to be added in the inventory:
```
[localhost]
127.0.0.1 ansible_become=0 ansible_connection=local
```
Root priviledge is not required on the control host.

## OS Management
The role supports several features to manage the operating system features:
- firewalld (*mariadb_firewalld_enabled*)
- selinux (*mariadb_selinux_enabled*)
- Fix incorrect mysql server (from old installations) - always enabled
- Upgrade all (other) OS packages (*mariadb_upgrade_other_packages*)
- Reboot if needed (supports both Red Hat and Debian) (*mariadb_reboot_if_needed*)
- Disable apparmor on Debian systems
- Disable SWAP
- Install logrotate profile and remove default logrotate profiles
All these features are enabled by default

## Testing:

- All changes should be backwards compatiblle with all previous versions. That's what makes a playbook idempotent. 
- The minimum amount of testing required for any change, is the following:
  - Does it run when everything is on the previous version?
  - Does it run when a part of the cluster is on the previous version?
  - Does it run on fresh nodes? (yum remove MariaDB-* galera && rm -rf /var/lib/mysql or apt purge mariadb-* && rm -rf /var/lib/mysql)
  - Does it run on both CentOS and Debian based systems?

## Running the role:
After specifying 1 or more nodes in cluster in your inventory, the role may be called like this:
```
- name: Installing a MariaDB galera cluster
  hosts: cluster
  become: 1
  serial: 1 # very important for multi node galera clusters to only bring down one node at once
  roles:
  - role: mariadb-mysql-galera
     mariadb_galera_cluster_ist_size: "4G"
     mariadb_version: "10.6"
     mariadb_expire_logs_days: 15
     mariadb_max_connections: 3000
     mariadb_innodb_io_capacity_max: 80000
     mariadb_transaction_isolation: "READ-COMMITTED"
     mariadb_update_local_users: True
     mariadb_login_user: root
     mariadb_local_users: "{{ local_system_users + local_developer_users }}"

- name: Installing stand-alone nodes not running Galera cluster
  hosts: node1,node2
  become: 1
  roles:
  - role: mariadb-mysql-galera
     mariadb_galera_enabled: False
     mariadb_version: "10.6"
     mariadb_expire_logs_days: 30
     mariadb_innodb_io_capacity_max: 20000
     mariadb_transaction_isolation: "READ-COMMITTED"
     mariadb_update_local_users: True
     mariadb_login_user: root
     mariadb_local_users: "{{ local_system_users + local_developer_users }}"
     mariadb_remove_unspecified_local_users_debug_mode: False
     mariadb_remove_unspecified_local_users_debug_mode: False
```

## Contributors
- Michaël de Groot (maintainer)
- Sarvesh Goburdhun
- Girish Mohabir
- Pranav Dondhea
