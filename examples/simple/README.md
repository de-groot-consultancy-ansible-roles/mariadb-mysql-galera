# Description
This example is the minimum configuration for the role to set up a galera cluster. Only (for the automation) required users are added or removed from the cluster when the automation runs.

# Installing
- Start to [install ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) 
- Then copy the files in this directory into a directory on your control host. 
- Install the dependencies, execute: `ansible-galaxy install -r requirements.yml`

# Usage
To install a Galera cluster, edit inventory.ini with your node information and make sure that ansible can login and has sudo access. The easiest way to log in safely is to run ansible after starting and enabling an SSH agent.

Then, execute ansible: `ansible-playbook -i inventory.ini playbook.yml`